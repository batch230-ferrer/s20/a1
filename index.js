//console.log("Kon'nichiwa sekai!");

let número = Number(prompt("Provide me a number"));

console.log("The number you provided is " + número);

for(número; número >=0; número--){
	if(número <= 50){
		console.log("The current value is at 50. Terminating the loop" );
		break;
	}
	if(número%10 === 0){
		console.log("The number is divisible by 10. Skipping the number " );
		continue;
	}
	if(número%5 === 0){
		console.log(número);
	}
}

let superCali = "supercalifragilisticexpialidocious";
let tomaConstante = "";

console.log(superCali);

for(let i=0; i<superCali.length; i++){
	if(superCali[i].toLowerCase() === 'a' || 
		superCali[i].toLowerCase() === 'e' ||
		superCali[i].toLowerCase() === 'i' ||
		superCali[i].toLowerCase() === 'o' ||
		superCali[i].toLowerCase() === 'u'){
			continue;
	}
	
	else{
		tomaConstante = superCali[i] + tomaConstante;	
	}
}
console.log(tomaConstante);